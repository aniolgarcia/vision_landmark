#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Exemple senzill per carregar una xarxa Yolo al mòdul DNN d'OpenCV i fer inferència.
Author: Aniol Garcia
"""

import cv2
import numpy as np

# Carreguem una imatge de prova
image = cv2.imread('testimg2.png')

'''
---------- CARREGAR LA XARXA ----------
Creem un objecte Net (https://docs.opencv.org/4.2.0/db/d30/classcv_1_1dnn_1_1Net.html)
    a partir dels pesos i la configuració. Evidentment els pesos són per una arquitectura
    concreta, que ha de coincidir amb la que es passa com a configuració. No saltarà cap 
    error si no quadren, simplement posarà els pesos com pugui i tirarà endavant, donant
    resultats erronis. Canvieu els paths dels fitxers segons connvingui.
'''

weights = 'yolo4-tiny_1280x768_gtsdb_single_class.weights'
config = 'yolo4-tiny_1280x768_single_class.cfg'

net = cv2.dnn.readNet(weights, config)


'''
Posteriorment, en fer inferència hem d'especificar els noms de les capes de les quals 
    volem llegir la sortida. Volem la sortida de la xarxa sencera, de manera que hem de
    trobar els noms de la capa de sortida.
    Per versions d'OpenCV >= 3.4.4 es pot fer servir directament 
    net.getUnconnectedOutLayersNames()
'''
ln = net.getLayerNames() 
lln = [ln[i-1] for i in net.getUnconnectedOutLayers()]


'''
---------- FENT INFERÈNCIA ----------
Abans de passar la imatge a la xarxa, s'ha de fer algunes modificacions al format, que
    es fan amb el blobFromImage. Els paràmetres són els següents:
    - image: la imatge que volem carregar, tal com la dóna OpenCV després de carregar-la
    - size: la mida que volem que tingui la imatge. Aquesta mida hauria de ser la mateixa
            que la mida d'entrada a la xarxa, especificada al fitxer .cfg (la mida que
            apareix als noms dels models del repo no sempre és la mida que espera la xarxa!
            És possible que sigui la mida de les imatges utilitzades per entrenar, que no
            siguin múltiples de 32 i s'hagi d'adaptar, canvis d'última hora... o 
            simplement que m'hagi despistat. Comprova sempre que la mida sigui la de 
            dins del fitxer .cfg). En cas de no ser la mateixa, enlloc s'especifica si es fa
            cas a la mida que es passa o si fa un resize internament per adaptar-la a 
            l'esperada. Els experiments que he fet semblen indicar que s'utilitza la mida que
            es passa (possiblement fent crop i obviant part de l'imatge), i en tot cas les 
            mides dels anchor boxes són sempre les del fitxer de configuració. Les mides del 
            blob que es passa a YOLO han de ser SEMPRE múltiples de 32.
    - mean: no utilitzat, és una 3-tupla de valors que es resten a cada canal de color.
    - scalefactor: valor pel qual es multiplica cada valor de píxel. OpenCV carrega les
            imatges amb l'interval de valors [0, 255], però Darknet espera el rang [0, 1],
            de manera que scalefactor sempre serà d'1/255.
    - swapRB: per algun motiu, quan es carrega una imatge en OpenCV, l'ordre dels canals
            de color és BGR, en lloc de RGB. Naturalment Yolo està entrenada en RGB, de 
            manera que cal fer la permutació dels canals R i B. Posant aquest paràmetre
            a true, el blob quedarà amb l'ordre de canals correcte.
 
El blob resultant és un array 4-dimensional amb estructura NCHW:
    - N: El nombre d'imatges al batch (en general només una en fer inferència)
    - C: Canals de color
    - H: Height
    - W: Width
'''

blob = cv2.dnn.blobFromImage(image, scalefactor=1/255.0, size=(1280, 736), swapRB=True)

net.setInput(blob) # Posem el blob a la capa d'entrada de la xarxa
outs = net.forward(lln) # Fem feed forward de la xarxa i guardem els outputs de la capa de sortida

'''
---------- MANIPULACIÓ DELS RESULTATS ----------
La sortida de la xarxa és una n-tupla on n és el nombre de capes de tipus YOLO de la xarxa sobre
la que es fa inferència (cada capa YOLO és responsable de fer les prediccions a partir d'unes 
mides d'anchor boxes concretes). Cada element de la tupla és un array de les deteccions de la capa
de YOLO corresponent. Cada detecció és un array en sí mateix amb (4 + n_classes) components. 

Les 4 primeres components de cada detecció corresponen a:
    - 0: La coordenada x del centre de la detecció
    - 1: La coordenada y del centre de la detecció
    - 2: L'amplada de la caixa que conté la detecció
    - 3: L'alçada de la caixa que conté la detecció
Totes aquestes mides són valors del rang [0,1] relatives a la mida del blob passat. Per exemple, si 
el valor de la primera component és 0.5, vol dir que la coordenada x del centre de la detecció és
al mig de la imatge (en aquest cas, 0.5*1280 = 640). Si el valor de la tercera component és 0.3 vol 
dir que la llargada de la caixa és 0.3 vegades la llargada del blob passat.

La resta d'n_classes components indiquen, per a cada classe, l'score d'aquella detecció.
'''

# Declarem les estructures on guardarem la informació de les deteccions
class_ids = []
confidences = []
boxes = []

# Threshold de l'score. Si una detecció no té cap classe amb score superior al threshold, s'ignora.
#   Els scores semblen sensibles a les mides de les anchor boxes, a vegades baixant el threshold
#   es detecten senyals de mides que abans no apareixien.
conf_threshold = 0.5

# Agafem les mides de la imatge sobre la que hem fet inferència, ho farem servir per calcular la
#   posició i mides absolutes de la detecció, en lloc de les relatives
(H,W) = image.shape[:2]


for out in outs:                            # Cada 'out' és l'array de deteccions d'una de les capes de YOLO
    for detection in out:                   # Cada detection és una detecció
        scores = detection[5:]              # De la component 5 fins al final són els scores de cada classe
        class_id = np.argmax(scores)        # Agafem l'índex amb valor més gran (i.e. la classe amb més score)
        confidence = scores[class_id]       # Agafem el valor de l'score més gran
        if confidence > conf_threshold:    # Si l'score passa el threshold,

            # Passem les mesures relatives a la mida del blob a absolutes en píxels
            center_x = int(detection[0] * W)
            center_y = int(detection[1] * H)
            w = int(detection[2] * W)
            h = int(detection[3] * H)
            
            # Calculem les coordenades de la cantonada superior esquerra, necessari per Non-Maximum Supression
            x = center_x - w / 2
            y = center_y - h / 2

            # Guardem, per separat (altre cop necessari per NMS) la classe, score i mides de cada detecció
            class_ids.append(class_id)
            confidences.append(float(confidence))
            boxes.append([x, y, w, h])


'''
---------- NON-MAXIMUM SUPRESSION ----------
Degut al funcionament intern de YOLO, hi haurà múltiples deteccions que detectin el mateix objecte
(és a dir, per un únic objecte hi haurà múltiples bounding boxes i scores). Per quedar-se amb una 
única detecció per objecte s'utilitza non-maximum supression. Se li han de passar:
    - boxes: array amb les mides de les deteccions
    - confidences: array amb els scores de les deteccions (ordenats igual que les boxes)
    - conf_threshold: Threshold dels scores, en principi redundant ja que les que no tenen un
        threshold superior a a quest ja no s'han utilitzat
    - nms_threshold: No s'especifica a la documentació, assumeixo que és el threshold per a 
        l'Intersection over Union de les caixes. Es pot provar de modificar.
    - eta (opcional): factor pel que es multiplica nms_threshold per la següent iteració. És 
        per fer threshold adaptatiu.
    - top_k (opcional): Només retorna les k millors deteccions.

Aquesta funció retorna un array d'índex corresponents a les millors deteccions.
'''

nms_threshold = 0.4
indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)


'''
---------- RESULTATS ----------
Arribats a aquest punt, per a cada índex i d'indices tenim que:
    - boxes[i] és un array [ x, y, w, h ] (les coordenades de la cantonada superior esquerra
        i les mides de la caixa)
    - class_ids[i] és l'índex de la classe de la detecció
    - confidences[i] és l'score de la detecció

Ara ja es poden tractar els resultats com convingui.
'''


# Això ja és superflu, només és per dibuixar els rectangles de les deteccions amb la classe i score
#   directament sobre les imatges.
for i in indices:
    box = boxes[i]
    x = int(box[0])
    y = int(box[1])
    w = int(box[2])
    h = int(box[3])
    cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
    text = "{}: {:.4f}".format(class_ids[i], confidences[i])
    cv2.putText(image, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)


cv2.namedWindow("RESULT", cv2.WINDOW_NORMAL)
cv2.imshow("RESULT", image)
while cv2.getWindowProperty("RESULT", cv2.WND_PROP_VISIBLE) > 0:
    if cv2.waitKey(100) > 0:
        break
cv2.destroyAllWindows()


