# Visual Landmark Detection

Repositori del treball final del grau d'Enginyeria Informàtica "Detecció de punts de referència en entorns urbans mitjançant visió per computador", realitzat a l'Institut de Robòtica i Informàtica Industrial.

La memòria i el codi entregats es poden trobar al dipòsit de la Universitat de Barcelona: https://diposit.ub.edu/dspace/handle/2445/199501

## Continguts del directori
El repositori conté els notebooks de Jupyter necessaris per entrenar les xarxes utilitzades, els programes per fer-ne la inferència, i múltiples fitxers auxiliars. A més, s'inclouen els models preentrenats i els fitxers de configuració de les xarxes.

**ATENCIÓ: Per norma general, els notebooks d'entrenament de les xarxes executen comandes que afecten i canvien l'estat del sistema on s'executen: instal·len/desintal·len  programari, descarreguen, compilen i modifiquen fitxers externs. NO ELS UTILITZEU DIRECTAMENT AL VOSTRE SISTEMA SENSE HAVER-LES COMPROVAT. Es recomana utilitzar-les només en entorns restringits (docker amb Debian, google colab, kaggle notebooks...).**

A continuació s'ofereix la llista de fitxers del repositori juntament amb una breu descripció:

```
├── birds_eye
│   ├── birds.py:                               Projectivitat de canvi de perspectiva i detecció de tapes de clavaguera (per color i àrea) sobre un stream de vídeo
│   ├── floor-landmarks_interactive.ipynb:      Notebook interactiu per extreure paràmetres de color pel filtre de clavagueres
│   └── floor-landmarks.ipynb:                  Notebook amb tot el procés de la projectivitat i detecció.
├── README.md:                                  Aquest fitxer
├── tools
│   ├── augmentation.py:                        Definició de funcions de manipulació d'imatge útils per augmentar dades
│   ├── color_picker.py:                        Eina interactiva per filtrar rangs de color
│   ├── exemple_inferencia_yolo.py:             Codi base molt comentat per fer inferència completa amb xarxes Yolo.
│   └── yolo_save_patch.py:                     Codi base per fer inferència amb xarxes Yolo. Aquest guarda els patches detectats com a imatge.
├── traffic_signs
│   ├── datasets:                               Datasets base (sense augmentació) utilitzats per entrenar les SVM. La resta de datasets són massa grans, en general els descarrega el propi notebook d'entrenament.
│   │   ├── MixedTS_colors.tar.xz:              Dataset de patches de senyals separats per color + categoria no senyal
│   │   └── MixedTS.tar.xz:                     Dataset bàsic
│   ├── full_CNN:                               Detecció i classificació en una sola xarxa.
│   │   ├── faster-rcnn_image_inference.py:     Script d'inferència per Faster-RCNN sobre imatges.
│   │   ├── faster-rcnn_training.ipynb:         Notebook d'entrenament per a xarxes FRCNN amb PyTorch. Conté el tractament de dades de GTSDB, funcions d'entrenament i validació. ALERTA: aquest notebook (i tots els d'entrenament a partir d'ara) fan canvis sobre el sistema on s'executen (i.e. instal·len paquets amb versions específiques, descarreguen i modifiquen fitxers). No estàn pensats per executar-se en local, sinó en l'entorn Kaggle Notebooks, per aprofitar les GPU que ofereixen. Preneu les precaucions pertinens abans d'executar-los.
│   │   ├── faster-rcnn_video_inference.py:     El mateix que abans però amb stream de vídeo
│   │   ├── models
│   │   │   ├── faster-rcnn_gtsdb-classes.pkl
│   │   │   ├── GTSDB.names
│   │   │   ├── GTSDB_supercategories.names
│   │   │   ├── yolo3_416x416_gtsdb_supercategories.weights
│   │   │   ├── yolo3_416x416_supercategories.cfg
│   │   │   ├── yolo3_640x384_gtsdb-classes.cfg
│   │   │   └── yolo3_640x384_gtsdb_gtsdb-classes.weights
│   │   ├── ssd_training.ipynb:                 Entrenament de SSD sobre PyTorch, similar al de FRCNN.
│   │   ├── yolo_opencv_video_inference.py:     Inferència de xarxes YOLO sobre streams de vídeo
│   │   └── yolo_training.ipynb:                Entrenament de YOLO sobre Darknet. Aquest notebook descarrega i modifica múltiples fitxers, compil·la Darknet i instal·la drivers d'NVIDIA i CUDA a més d'altres paquets. NO L'EXECUTEU DIRECTAMENT SOBRE EL VOSTRE SISTEMA.
│   └── multi_stage:                            Detecció i classificació separades
│       ├── classifier_training.ipynb:          Entrenament d'una xarxa classificadora
│       ├── faster-rcnn+classifier.py:          Inferència amb FRCNN com a detectora i la xarxa de sobre com a classificadora
│       ├── faster-rcnn+spatial-classifier.py:  FRCNN com a detectora i una altra xarxa com a classificadora (no utilitzat)
│       ├── faster-rcnn_training.ipynb:         Entrenament de FRCNN amb una sola classe o supercategories
│       ├── lm_filters.py:                      Implementació dels filtres de Leung-Malik
│       ├── models
│       │   ├── blue_svm.dat
│       │   ├── blue_svm_plus.dat
│       │   ├── classifier_gtsdb.pkl
│       │   ├── classifier.py
│       │   ├── faster-rcnn_single_class.pkl
│       │   ├── hog_of_mask_svm.dat
│       │   ├── red_svm.dat
│       │   ├── red_svm_plus.dat
│       │   ├── single_class.names
│       │   ├── yellow_svm.dat
│       │   ├── yellow_svm_plus.dat
│       │   ├── yolo3_1280x768_gtsdb_single_class.weights
│       │   ├── yolo3_1280x768_single_class.cfg
│       │   ├── yolo3_640x384_gtsdb_single_class.weights
│       │   ├── yolo3_640x384_single_class.cfg
│       │   ├── yolo4-tiny_1280x768_gtsdb_single_class.weights
│       │   ├── yolo4-tiny_1280x768_single_class.cfg
│       │   ├── yolo4-tiny_640x384_gtsdb_single_class.weights
│       │   ├── yolo4-tiny_640x384_single_class.cfg
│       │   ├── YOLOv3_1280x736_anchors_categories_best.weights
│       │   ├── YOLOv3_1280x736_anchors_categories.cfg
│       │   └── YOLOv3_640x384_anchors_categories.cfg
│       ├── __pycache__
│       │   └── lm_filters.cpython-310.pyc
│       ├── spatial-classifier_training.ipynb:  Entrenament del classificador espaial (no utilitzat)
│       ├── visual-classification.ipynb:        Notebook d'entrenament de les SVM. Conté definicions i funcions de càlcul de múltiples descriptors d'imatge, no tots utilitzats finalment. 
│       ├── yolo_multi_svm.py:                  Inferència amb YOLO com a detector i tres SVM (una per cada color).
│       ├── yolo_svm.py:                        Inferència amb YOLO com a detector i una única SVM classificadora (hog_of_mask_svm.dat, principalment). No utilitzat finalment.
│       └── yolo_training.ipynb:                Entrenament de YOLO amb una sola classe o supercategories
└── vanishing_point
    ├── live_vanishing_point.py:                Càlcul del punt de fuga a un stream de vídeo
    └── vanishing_point.ipynb:                  Notebook de proves de concepte i procés de càlcul pel detector.
```
